/* eslint-env browser, webextensions */
/* eslint no-console: 0 */

"use strict";

/**
 * Marks an iFrame to visualize CV content
 * @param {string} searchString What to look for in the src attribute.
 * @param {number} tries number of tries to attempt.
 */
function markFrame(searchString, tries = 0)
{
  let elements = document.querySelectorAll(
    `iframe[src*="${searchString}"]`
  );

  // If iFrames are side-loaded and tag creation is delayed, we try up to 600
  // times in 50ms intervals (30 seconds)
  if (elements.length < 1 && tries < 600)
  {
    window.setTimeout(() =>
    {
      markFrame(searchString, tries + 1);
    }, 50);
    return;
  }

  elements.forEach(el =>
  {
    el.style.border = "3px dotted blue";
    el.style.outline = "3px dotted red";
  });
}

/**
 * Marks an image to visualize CV content
 * @param {string} searchString What to look for in the src attribute.
 * @param {number} tries number of tries to attempt.
 */
function markImage(searchString, tries = 0)
{
  let elements = document.querySelectorAll(
    `img[src*="${searchString}"]`
  );

  // If images are side-loaded and tag creation is delayed, we try up to 600
  // times in 50ms intervals (30 seconds)
  if (elements.length < 1 && tries < 600)
  {
    window.setTimeout(() =>
    {
      markImage(searchString, tries + 1);
    }, 50);
    return;
  }

  elements.forEach(el =>
  {
    el.style.border = "3px dotted blue";
    el.style.outline = "3px dotted red";
  });
}

/**
 * Marks elements based on URL and element type
 * Ping and script resources can't be marked obviously, for everything else
 * we try to identify img and iFrame elements
 * @param {string} url the URL of the frame to mark.
* @param {string} type the type of content to mark.
 */
function markElement(url, type)
{
  let searchString = url.replace(/https?:\/\/[^/]+\/([^?]*).*/, "$1");

  switch (type)
  {
    case "image":
      markImage(searchString);
      break;

    case "media":
      markImage(searchString);
      break;

    case "sub_frame":
      markImage(searchString);
      markFrame(searchString);
      break;

    case "xmlhttprequest":
      markImage(searchString);
      markFrame(searchString);
      break;

    case "script":
      break;

    case "ping":
      break;

    default:
      console.log(`Don't know how to mark ${type} element with URL ${url}`);
  }
}

// Message Receivers
chrome.runtime.onMessage.addListener(
  (request, sender, sendResponse) =>
  {
    switch (request.action)
    {
      case "markElement":
        markElement(request.url, request.type);
        sendResponse({});
        break;

      case "debug":
        console.log("Debug msg:", request.msg);
        sendResponse({});
        break;

      default:
        console.log("Got an unexpected message:", request.action);
        sendResponse({});
    }
  });
