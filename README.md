Adblock Plus Developer companion
================================

This extension is aimed towards ad blocking software developers and ad
blocking filter authors.

While it is tailored for Adblock Plus, it is likely to be useful to
developers working on other ad blocking software.

Modules
-------

The Adblock Plus Developers companion has several different
modules. View it as distinct feature sets packaged together.

# uncvr

This module analyze the requests to load assets in order to guess
which circumvention provider is used on the current website.

Installation
------------

Currently you have to install the extension unpacked from this
directory.

Contribute
----------

The repository is hosted at:

https://gitlab.com/eyeo/auxiliary/abp-dev

